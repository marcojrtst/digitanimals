﻿  $(function(){

      $(document).ready(function () {

          var roles = JSON.parse($.ajax({
              type: 'GET',
              url: '/list/roles',
              dataType: 'json',
              global: false,
              async:false,
              success: function(data) {
                  return data;
              }
          }).responseText);

          $( "#DateOfBirth" ).datepicker({
              changeYear: true,
              changeMonth: true,
              yearRange: '1930:2002'
          });
          $( "#Role" ).autocomplete({
              source: roles
          });



          jQuery("#Location").autocomplete({
              source: function (request, response) {
                  jQuery.getJSON(
                          "http://gd.geobytes.com/AutoCompleteCity?callback=?&q="+request.term,
                      function (data) {
                          response(data);
                      }
                  );
              },
              minLength: 3,
              select: function (event, ui) {
                  var selectedObj = ui.item;
                  jQuery("#Location").val(selectedObj.value);
                  return false;
              },
              open: function () {
                  jQuery(this).removeClass("ui-corner-all").addClass("ui-corner-top");
              },
              close: function () {
                  jQuery(this).removeClass("ui-corner-top").addClass("ui-corner-all");
              }
          });
          jQuery("#Location").autocomplete("option", "delay", 100);

      });
  });
  function CreateUser()
  {
      var location = $('#Location').val().split(',');
          var user = {
          "name" : $('#Name').val(),
          "email" : $('#Email').val(),
          "gender" : $("input[name='Gender']:checked").val(),
          "city" : location[0].trim(),
          "state" : location[1].trim(),
          "country" : location[2].trim(),
          "birthdate" : $('#DateOfBirth').val(),
          "role" : $('#Role').val(),
          "password" : $("#Password1").val()
      };
  console.log(user);

      $.post("/add/user", user, function(data) {
          alert(data);
      }).fail(function(err){
      });

  }