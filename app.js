
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var roles = require('./routes/roles');
var http = require('http');
var path = require('path');
var app = express();
var md5 = require('MD5');
var config = require('./config.json');
var db = require('mongojs').connect(config.mongoDB, config.mongoDBcollections);
console.log(db);

// all environments
app.set('port', process.env.PORT || 5000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
//app.use('/css',express.static(path.join(__dirname, 'public/css')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
app.get('/', routes.index);
app.get('/users', user.list);
app.post('/add/:type', function(req, res) {
    if(req.param("type") =='user') {
        buddys = db.collection('buddys');
        var dta = {  type: "buddy",
            accountInfo: {
                kind: 'DA',
                country : req.body.country,
                state : req.body.state,
                city : req.body.city,
                name: req.body.name,
                gender : req.body.gender,
                birthdate : req.body.birthdate,
                email: req.body.email,
                role: req.body.role,
                password: md5(req.body.password),
                dateCreation: Date(),
                registerStatus: 1,
                status: 'active'
            }
        }
        console.log(dta);
        buddys.insert(dta, function (err, doc) {
            console.log(doc);
            res.send(doc._id);
        });
    }
});
app.get('/list/:type', function(req, res) {
    if(req.param("type") =='roles') {
        db.generic.find({type:'roles'}).toArray(function(err, item) {
            var i = item[0].roles;
            res.send(i);
            console.log(i);
        });
    }
});
http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
